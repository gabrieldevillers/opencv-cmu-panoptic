#include <iostream>
#include <string>
#include <unordered_map>
#include "boost/filesystem.hpp"

#include <opencv2/opencv.hpp>
#include "opencv2/viz/viz3d.hpp"

cv::Mat unserialize_2d_float_mat(cv::FileNode node, int rows, int cols) {
    // These two lines do not work while I think they should
    // (http://docs.opencv.org/3.2.0/d4/da4/group__core__xml.html)
    // cv::Mat K;
    // (*camera_it)["K"] >> K;
    auto vect = std::vector< std::vector<float> >();
    node >> vect;
    cv::Mat M = cv::Mat(rows, cols, CV_32F);
    for (int row = 0;  row < 3; row++) {
        M.row(row) = cv::Mat(vect[row]).t();
    }

    return M;
}

cv::Point3d convertMatx31dToPoint3d(cv::Matx31d mat) {
    return cv::Point3d(mat(0,0), mat(1,0), mat(2,0));
}

cv::Vec3d convertMatx31dToVec3d(cv::Matx31d mat) {
    return cv::Vec3d(mat(0,0), mat(1,0), mat(2,0));
}

class Camera {
    public:
        Camera(cv::FileNode const camera_node) {
            name_ = std::string(camera_node["name"]);
            type_ = std::string(camera_node["type"]);
            K_ = unserialize_2d_float_mat(camera_node["K"], 3, 3);
            R_ = unserialize_2d_float_mat(camera_node["R"], 3, 3);
            t_ = unserialize_2d_float_mat(camera_node["t"], 3, 1);
        };
        std::string getName() const {
            return name_;
        };
        std::string getType() const {
            return type_;
        };

        // https://en.wikipedia.org/wiki/Camera_resectioning
        // and https://math.stackexchange.com/questions/82602
        cv::Matx31d getWorldPosition() const {
            return - R_.t() * (1E-2 * t_); // from cm to meter
        }
        cv::Matx31d getOrient() const {
            return R_.t() * cv::Matx31d(0.0, 0.0, 1.0);
        }

        cv::Matx31d getUpVector() const {
            return R_.t() * cv::Matx31d(0.0, 1.0, 0.0);
        }

        cv::Matx31d getArrowTip() const {
            return getWorldPosition() + 0.2 * getOrient();
        }

        // required to insert in a std::set:
        bool operator <(const Camera &rhs) const
        {
            return (name_ < rhs.name_);
        }

    private:
        std::string name_;
        std::string type_;
        cv::Matx33d K_;
        cv::Matx33d R_;
        cv::Matx31d t_;
};

int main(int argc, char** argv )
{
    if ( argc != 2 )
    {
        std::cout << "usage: "
            << argv[0] << " <CMU Panoptic dataset directory>" << std::endl;
        return -1;
    }

    // Get the path to the dataset directory
    boost::filesystem::path path(argv[1]);
    std::cout << "Using directory: " << argv[1] << std::endl;

    // Get the name of the dataset from the path leading to it
    // TODO: improve with filesystem::absolute,
    // and filesystem::canonical else you cannot use a path ending with ".",
    // (else ask the user to specify the path to the calibration's JSON)
    std::string setname = path.parent_path().filename().string();
    std::cout << "Assuming that the dataset name is: " <<  setname << std::endl;

    // Compute path to the required calibration data
    boost::filesystem::path calibration_path(path.string()
            + "calibration_" + setname + ".json");
    if (!boost::filesystem::exists(calibration_path)) {
        std::cout << "Sorry, could not found calibration file "
            << calibration_path.string() << std::endl
            << "Exiting." << std::endl;
        return -1;
    }

    // Open the required calibration's JSON
    // JSON import was introduced in OpenCV 3.2 I think
    cv::FileStorage calibration_storage;
    try {
        calibration_storage = cv::FileStorage(calibration_path.string(),
                cv::FileStorage::READ);
    } catch (const std::exception& e) {
        std::cout << e.what();
        return -1;
    }

    // Compute path to the optional list of excluded cameras
    boost::filesystem::path exclusion_path(path.string()
            + "excluded_cameras_" + setname + ".json");
    std::set<std::string> excluded_cameras = std::set<std::string>();

    // Open the optional camera exclusion's JSON
    // allows to read a json containing '{"excludedCameras": ["00_31","14_18"]}'
    try {
        auto exclusion_storage = cv::FileStorage(exclusion_path.string(),
                cv::FileStorage::READ);
        cv::FileNode exclusion_node = exclusion_storage["excludedCameras"];
        for (cv::FileNodeIterator it = exclusion_node.begin();
                it != exclusion_node.end();
                it++)
        {
            excluded_cameras.insert((*it).string());
        }
    } catch (const std::exception& e) {}

    // Load all cameras keeping the non-excluded one
    auto cameras = std::set<Camera>();
    cv::FileNode camera_nodes = calibration_storage["cameras"];
    for(cv::FileNodeIterator camera_it = camera_nodes.begin();
            camera_it != camera_nodes.end();
            camera_it++) {

        Camera camera(*camera_it);
        if (excluded_cameras.count(camera.getName()) == 0) {
            cameras.insert(camera);
        }
    }

    // Load all videos
    auto videos = std::map<Camera, cv::VideoCapture>();
    for (const auto& camera : cameras) {
        boost::filesystem::path video_path = path
            / (camera.getType()+"Videos")
            / (camera.getType() + "_" + camera.getName() + ".mp4");
        videos.insert({camera, cv::VideoCapture(video_path.string())});
    }

    // Graphic stuff with VTK
    // Open a window and place a coordinate indicator
    cv::viz::Viz3d window = cv::viz::Viz3d("Visualize hd cameras");
    window.showWidget("Coordinate Widget", cv::viz::WCoordinateSystem());

    // Initialize cameras related widgets
    auto image_widgets = std::map<Camera, cv::viz::WImage3D>();
    for (const auto& camera : cameras) {
        cv::viz::Color arrow_color;
        if (camera.getName().compare("vga") == 0) {
            arrow_color = cv::viz::Color::blue();
        } else if (camera.getType().compare("hd") == 0) {
            arrow_color = cv::viz::Color::green();
        } else {
            arrow_color = cv::viz::Color::red();
        }

        if (camera.getType().compare("hd") == 0) {
            cv::viz::Widget arrow = cv::viz::WArrow(
                    convertMatx31dToPoint3d(camera.getWorldPosition()),
                    convertMatx31dToPoint3d(camera.getArrowTip()),
                    0.05,
                    arrow_color);

            window.showWidget(camera.getName() + "arrow", arrow);


            cv::viz::Widget arrow_text = cv::viz::WText3D(camera.getName(),
                    convertMatx31dToPoint3d(camera.getWorldPosition()),
                    0.08,
                    true);
            window.showWidget(camera.getName() + "arrow_text", arrow_text);

            cv::Mat image_read;
            bool result = videos[camera].read(image_read);
            if (!result) {
                std::cout << camera.getName()
                    << ": could NOT read" << std::endl;
                continue; // do not place an empty image
            }
            cv::Mat image;
            image_read.convertTo(image, CV_8UC3);

            cv::viz::WImage3D image_widget = cv::viz::WImage3D(image,
                    cv::Size2d(1.1,1.1),
                    convertMatx31dToVec3d(camera.getArrowTip()),
                    convertMatx31dToVec3d(camera.getOrient()),
                    convertMatx31dToVec3d(camera.getUpVector()));

            image_widgets.insert({camera, image_widget});

            window.showWidget(camera.getName() + "image", image_widget);
        }
    }

    // Event loop for 1 millisecond
    window.spinOnce(1, true);
    bool videos_ended = false;
    while(!window.wasStopped() && !videos_ended)
    {
        for(auto iter = image_widgets.begin();
                iter != image_widgets.end();
                ++iter) {
            Camera camera = iter->first;
            cv::viz::WImage3D image_widget = iter->second;

            cv::Mat image_read;
            bool result = videos[camera].read(image_read);
            if (!result) {
                std::cout << camera.getName()
                    << ": could NOT read, exiting" << std::endl;
                videos_ended = true;
                continue; // do not attempt placing empty image
            }
            cv::Mat image;
            image_read.convertTo(image, CV_8UC3);
            image_widget.setImage(image);
        }

        window.spinOnce(1, false);
    }

    return 0;
}
