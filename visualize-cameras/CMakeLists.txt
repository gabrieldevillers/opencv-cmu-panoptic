cmake_minimum_required(VERSION 2.8)
project( visualize-cameras )

# ask for C++11 (source: https://stackoverflow.com/a/25836953/4669135)
include(CheckCXXCompilerFlag)
CHECK_CXX_COMPILER_FLAG("-std=c++11" COMPILER_SUPPORTS_CXX11)
CHECK_CXX_COMPILER_FLAG("-std=c++0x" COMPILER_SUPPORTS_CXX0X)
if(COMPILER_SUPPORTS_CXX11)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
elseif(COMPILER_SUPPORTS_CXX0X)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
else()
    message(STATUS "The compiler ${CMAKE_CXX_COMPILER} has no C++11 support.
    Please use a different C++ compiler.")
endif()

# I use a manually compiled and installed version of OpenCV,
# because I need version 3.2.0+ to open .json files
# I have set my environnement variable OPENCV_INSTALL_DIR to approriate dir
if(DEFINED ENV{OPENCV_INSTALL_DIR})
    # Did not work: SET("OpenCV_DIR" "path-to-opencv-install-dir")
    find_package(OpenCV 3.2 REQUIRED PATHS $ENV{OPENCV_INSTALL_DIR})
else()
    message("OPENCV_INSTALL_DIR not set, searching in default location(s)")
    find_package(OpenCV 3.2 REQUIRED)
endif(DEFINED ENV{OPENCV_INSTALL_DIR})

find_package( Boost 1.46 REQUIRED COMPONENTS system filesystem )
include_directories( ${OpenCV_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS} )
add_executable( visualize-cameras visualize-cameras.cpp )
target_link_libraries( visualize-cameras ${OpenCV_LIBS} ${Boost_LIBRARIES} )
