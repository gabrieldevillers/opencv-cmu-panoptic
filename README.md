# OpenCV programs in C++ using CMU's Panoptic Dataset
Small programs that I use to learn OpenCV. May contain bugs.

These programs expect to be run on [CMU's Panoptic video
dataset](http://domedb.perception.cs.cmu.edu/index.html).

Tested on Linux only.

## Programs
* vizualize-cameras

>Loads camera informations from the JSON,
>compute their positions from calibration data,
>uses VTK to place arrows in space to symbolize hd cameras,
>and displays any available hd video frame by frame at the appropriate location
>(very slow).
>
>Takes 1 parameter: the path to the folder containing the dataset
>(with a name such as 141217_pose1).
>The name of the folder must be the name
>of the dataset (it already is if you downloaded the dataset with the official
>script).


## Dependencies
* OpenCV 3.2 (3.2 is required to open JSON files with **`cv::FileStorage`**,
but you could write a parser yourself)
* Boost 1.46
* CMake 2.8

## Build
If you have manually installed OpenCV 3.2
you may have to set the environnement variable **`OPENCV_INSTALL_DIR`**
to the appropriate directory.
```
    cd *subdirectory-of-the-program*
    mkdir build && cd build
    cmake ..
    make
```

## Licence
BSD 2-clause "Simplified" License
